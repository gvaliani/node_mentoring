// CALL PACKAGES WE NEED
// ===============================================================
var express 			= require('express');
var app 					= express();
var exphbs  			= require('express3-handlebars');
var path 					= require('path');
var favicon 			= require('static-favicon');
var logger 				= require('morgan');
var cookieParser 	= require('cookie-parser');
var bodyParser 		= require('body-parser');
var session      	= require('express-session');
var debug 				= require('debug')('my-application');
var routes 				= require('./config/routes');
var server 				= app.listen(process.env.PORT || 3000, function() {
  										debug('Express server listening on port ' + server.address().port);
										});
var io = require('socket.io')(server);
var passport 			= require('passport');
var session      	= require('express-session');

var hbs = exphbs.create({
	layoutsDir: path.join(__dirname, 'app/views/layouts'),
	extname: ".hbs"
});
// BASE SETUP
// ================================================================
// Pass passport for configuration.
require('./config/passport')(passport);
// Set Handlebars render for .hbs files.
app.engine('hbs', hbs.engine);
// Default view render.
app.set('view engine', 'hbs');
// View files directory.
app.set('views', path.join(__dirname, 'app/views'));

app.use(favicon());
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ secret: 'tottina' }));
app.use(passport.initialize());
app.use(passport.session());

// ROUTES DEFINITIONS
// ================================================================
routes(app, passport);

// ERROR HANDLING
// ================================================================
/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error/error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error/error', {
		message: err.message,
		error: {}
	});
});


// TEMPORARY SOCKET.IO SETTING. LOOK FOR A BETTER PLACE TO DO THIS
// ==================================================================
// var io = require('socket.io').listen(server);
// io.sockets.on('connection', function (socket) {
//   socket.emit('news', { hello: 'world' });
//   socket.on('my other event', function (data) {
// 	console.log(data);
//   });
// });
io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('chat message', function(msg){
  	console.log('msg from client: ', msg)
    io.emit('chat message', msg);
  });

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});