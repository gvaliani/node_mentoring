module.exports = function (app, passport) {
	var express = require('express');
	var adminRouter = express.Router();
	var frontendRouter = express.Router();


	//Only logged users can access to admin section
	app.use('/admin', function(req, res, next){
		// if(req.isAuthenticated()){
			next();
		// }

		// res.send(401, { message: "You don't have access to this section." });
	})

	/* ============================ */
	/* Front end router							*/
	/* ============================ */

	var basePath = '../app/controllers/';

	frontendRouter.get('/', function(req, res){
		res.render('index/index', { layout: 'application' });
	});
	var index = require(basePath + 'indexController');
	app.use('/', frontendRouter);



	/* ============================ */
	/* Admin router									*/
	/* ============================ */
	
	var adminBasePath = '../app/controllers/admin/';

	//Render admin layout
	adminRouter.get('/', function(req, res, next) {
		console.log('isAuthenticated: ', req.isAuthenticated());
		console.log('req.session: ', req.session);
		console.log('req.user: ', req.user);
		console.log('====================')
		

	  res.render('admin/index', { layout: 'admin' });
	});

	adminRouter.post('/login', passport.authenticate('local-login', {
		successRedirect: '/ok',
		failureRedirect : '/failure'
	}));

	// app.post('/admin/login', function(req, res, next){
	// 	passport.authenticate('local', function(err, user, info){
	// 		console.log('authenticate arguments: ', arguments)
	// 		console.log('====================')
	// 		next();
	// 	})(req, res, next);
	// });

	// adminRouter.get('/users', function(req, res, next){
	// 	console.log('isAuthenticated: ', req.isAuthenticated());
	// 	console.log('req.session: ', req.session);
	// 	console.log('req.user: ', req.user);
	// 	console.log('====================')
	// 	next();
	// });

	/* === USER ROUTES === */
	var usersControllerClass = require(basePath + 'usersController');
	var users = new usersControllerClass(passport);
	// Display a list of all users
	adminRouter.get('/users', users.index);
	//Display a specific user
	adminRouter.get('/users/:id', users.show);
	//Search users
	adminRouter.get('/users/search', users.search);
	//Create a new user
	adminRouter.post('/users', function(req, res, next){
		
		passport.authenticate('local-createUser', function(err, user, info){
			if (!req.body.email || !req.body.password){
				res.send('400', { message: 'Empty credencials.' });
				return;
			}

			if(err){
				res.send('400', { message: err.message });
				return;
			}

			res.json(user);
			
		})(req, res, next);
	});
	//Update a specific user
	adminRouter.put('/users/:id', users.update);
	//Delete a specific user
	adminRouter.delete('/users/:id', users.destroy);

	app.use('/admin', adminRouter);
}