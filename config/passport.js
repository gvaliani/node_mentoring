var LocalStrategy = require('passport-local').Strategy;
var User = require('../app/models/user.js'); //Mongoose model

module.exports = function(passport){
	passport.serializeUser(function(user, done) {
	  done(null, user._id);
	});
	
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
      done(err, user);
    });
	});

	passport.use('local-login', new LocalStrategy({
			passReqToCallback : true
		},
		function(req, username, password, done){
			console.log('username: ', username)
			console.log('password: ', password)
			console.log('done: ', done)
			console.log('====================')
			return done(null, { _id: 1, username: 'Joe', password: 'schmo'});
		}
	));
	
	passport.use('local-createUser', new LocalStrategy({
			usernameField : 'email',
      passwordField : 'password',
			passReqToCallback : true
		},
		function(req, email, password, done){
			User.findOne({ 'local.email' :  email }, function(err, user) {
				if (err) return done(err);
				if (user) return done({ message: 'That email is already taken.' }, false);
				
				var newUser = new User();
				newUser.name 						= req.body.name;
				newUser.last_name 			= req.body.last_name;
				newUser.role 						= req.body.role;
				newUser.local.email 		= email; 
				newUser.local.password 	= newUser.generateHash(password);

				newUser.save(function(err){
					if(err && (err.name == 'ValidationError' || err.name == 'CastError') ){
						var message = err.message || '';

						if(err.errors){
							for(attr in err.errors){
								message = message.concat(err.errors[attr].message+'<br>');
							}
						}

						return done({ message: message });
					}

					return done(null, newUser);
				});
			});
		}
	));
}