adminApp.controller('usersController', ['$scope', 'toaster', 'usersService', function($scope, toaster, usersService){
	var self = this;
	this.selectedIndex;
	
	this.showErrors = function(res){
		// var errors = '';
		// for(err in res.data){
		// 	errors = errors.concat(res.data[err].message+'<br>');
		// }
		console.log(res)
		toaster.pop('error', '', res.message, 15000, 'trustedHtml');
	}

	$scope.selectUser = function(index, user){
		self.selectedIndex = index;
		$scope.selectedUser = angular.copy(user);
	};

	$scope.save = function(){
		var user = $scope.selectedUser || {};

		if(user._id){// Edit item
			usersService.put(user, 
				function(editedUser){// Success callback
					$scope.users[self.selectedIndex] = editedUser;
					$scope.selectedUser = {};
				},
				function(res){
					self.showErrors(res.data);
				}
			);
		}else{// Create item
			usersService.save(user, 
				function(newUser){// Success callback
					$scope.users.push(newUser);
					$scope.selectedUser = {};
				},
				function(res){// Failure callback
					self.showErrors(res.data);
				}
			);
		}
	};

	$scope.delete = function(index, user){
		user.$delete(function(){
			$scope.users.splice(index, 1);
		});
	};

	$scope.users = usersService.query();
}]);