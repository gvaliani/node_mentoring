adminApp.factory('usersService', ['$resource', 
	function($resource){
		return $resource('../admin/users/:id', {id: '@_id'}, {
			put: { method: 'PUT' }
		});
	}
]);