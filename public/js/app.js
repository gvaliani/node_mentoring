var adminApp = angular.module('adminApp', ['ngRoute', 'ngResource', 'toaster']);

adminApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/users', {
        templateUrl: '../partials/users.html',
        controller: 'usersController'
      }).
      otherwise({
        redirectTo: '/users'
      });
  }]);