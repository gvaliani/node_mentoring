# Welcome

This repository is just to learn how to make a web application with Node.js (with Express.js, Socket.io and MongoDB) and Twitter bootstrap.

## Installation

### Node
* Download and install node.js from here: http://nodejs.org/download/

### Mongodb
* Download mongodb from here: http://www.mongodb.org/downloads
* Installation guide: http://docs.mongodb.org/manual/installation/
* Set mongodb bin directory in to your PATH var.
* Create a directory named 'Data' inside to your project directory, at the same level that app.js

## Setup
* Go to directory where you downloaded this repository.
* Run 'npm install' to download the node modules needed.
* Run 'grunt server' to put up the node server and mongo daemon.

#### Notes
The directory layout was based on https://gist.github.com/lancejpollard/1398757