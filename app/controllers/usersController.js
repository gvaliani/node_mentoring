var User = require('../models/user.js'); //Mongoose model
var _ = require('underscore');

module.exports = function(passport){

	/* GET users listing. */
	this.index = function(req, res) {
		this.search(req, res);
	}.bind(this);

	/* Search users */
	this.search = function(req, res){
		var query = req.query;
		var search = {};
		var options = {};
		options.limit = parseInt(query.limit) || 100;
		options.skip = (parseInt(query.page) || 1) * options.limit - options.limit;

		//Avoiding pagination values be used as a search parameters.
		delete query.limit;
		delete query.page;

		for(attr in query){
			search[attr] = new RegExp(query[attr], 'i');
		}

		User
		.find(search, null, options)
		.exec(function(err, users){
			if(err) return res.send(err);
			
			res.json(users);
		});
	};
	
	/* Create user. */
	this.create = function(req, res, next) {
		
	  // var newUser = new User(req.body);
	  // newUser.save(function(err, product, numberAffected){
	  // 	if(err){
	  // 		console.log('======');
	  // 		console.log(err);
	  // 		console.log('======');
	  // 	}else{
	  // 		res.json(newUser);
	  // 	}
	  // });
	};
	
	/* Get specific user. */
	this.show = function(req, res) {
		User.findById(req.params.id, function (err, user) {
		  if(err) return res.send(err);
		  
		  res.json(user);
		});
	};
	
	/* Update user. */
	this.update = function(req, res) {
 //  	User.findById(req.params.id, function (err, user) {
 //  		delete req.body._id; //This is to avoid _id overwrite by a body param.
 //  		user = _.extend(user, req.body);

 //  	  if(err) return res.send(err);
 //  	  if(!user) return res.json({ message: "User not found." });
	  	
	//   	user.save(function(err, user){
	//   		if (err) return res.send(err);

	//   		res.json(user);
	//   	});
  	  
 //  	});
	};
	
	/* Delete user. */
	this.destroy = function(req, res) {
  	User.remove(
  		{ _id: req.params.id },
  		function (err) {
	  	  if(err) return res.send(err);

				res.json({ message: 'Succefully deleted.' });
  		}
  	);
	};
};