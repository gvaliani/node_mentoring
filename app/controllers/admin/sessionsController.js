var express = require('express');
var router = express.Router();

// match '/signup',  to: 'sessions#new'
//  match '/signin',  to: 'sessions#new',     via: :get
//  match '/signin',  to: 'sessions#create',  via: :post
//  match '/signout', to: 'sessions#destroy', via: :delete
router.get('/', function(req, res) {
  res.render('layouts/login');
});

router.get('/login', function(req, res) {
  res.render('layouts/login');
});

module.exports = router;