var mongoose = require('../../lib/db');
var bcrypt   = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
	name 			: { type: String },
	last_name : { type: String },
	role 			: { type: Number, required: true, default: 1 }, 
	local 		: {
		email 	: { type: String, required: true, unique: true, lowercase: true, match: /\S+@\S+\.\S/ },
		password: { type: String, required: true }
	}
});

// generating a hash
userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);